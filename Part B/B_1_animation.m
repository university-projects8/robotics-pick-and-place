
%% Initializations
global joints_0;
global RobotArm;
global Thetas;
global HomoRobotArm;
% Constraints
a_max=0.7;
v_max=0.35;
% Create the 6-DOF robot arm
RobotArm=lwr_create();      %SerialLink Object

% This is the desired orientation during the whole movement. It's basically the initial
% orientation which we want to keep it steady.
q_desired=[0,0,1,0];    %(1x4)

% The initial position of the tool and the respective homogeneous transformation matrix 
pE_0=[-0.3397, 0.5088, 0.4049]';    %(3x1)
   
% The intermediate - "pick" position.
pE_1=[-0.0242, 0.6822, 0.1]';    %(3x1)

%The final position of the tool and the respective homogeneous matrix
pE_2=[0.3884    0.5967    0.18]';

% The initial configuration of the joints
joints_0=[2.1595, -0.6695, 1.2719, 3.1416, 1.2002, -0.9821]';     %(6x1)

% Timestep for the integration
Ts=2*1e-3;

% Total Time for the "Place" Trajectory
tf=3;

% The homogeneous transformation matrix at pE_0
T_E_0=[quat2rotm(quaternion(q_desired)),pE_0;zeros(1,3),1];    %(4x4)

% The homogeneous transformation matrix at pE_1
T_E_1=[quat2rotm(quaternion(q_desired)),pE_1;zeros(1,3),1];    %(4x4)

% The homogeneous transformation matrix at pE_final
T_E_final=[quat2rotm(quaternion(q_desired)),pE_2;zeros(1,3),1];    %(4x4)

Traj=load('trajectory3rdOrder.mat');

% For the second part we have predefined trajectories for the velocity and position.
velocity=[Traj.v_total];
position=[Traj.s_total];
tspan_init=[Traj.t_total];

% We will use Euler Integration with timestep 2ms, so we will interpolate the trajectory
% in the same timespan with timestep of 2ms (instead of 1e-6 used during design).
tspan_desired=0:Ts:tf;

velocityCylinder=spline(tspan_init,velocity,tspan_desired);
positionCylinder=spline(tspan_init,position,tspan_desired);

%This position of the trajectory is for frame A, which is located at the bottom of the 
% cylinder. We add the respective height of the cylinder in order to find the Tool's
% position.
h=0.1;
positionE=positionCylinder+[0;0;h];

%Velocity must be transformed to the edge tool orientation as well. Since we know that
%both orientations are kept constant during the movement, we can use a constant rotation
%matrix R_E0 which is calculated based on the desired quaternion we want for A. So, we
%multiply from the right side the velocity vector in order to express it in the Tool's
%frame.
velocityE=zeros(3,size(tspan_desired,2));
R_E0=quat2rotm(quaternion(q_desired))';
for k=1:size(tspan_desired,2)
   velocityE(:,k)=R_E0*velocityCylinder(:,k);
end
%% Trajectories Simulation
%counter for total repetitions
global ctr;
ctr=1;

% 1st PART OF "PICK" MOVEMENT
%The desired position 1. We will align the Tool with the Cylinder in axes x and y, so
%that it can correctly be picked afterwards.
pE_d1=[-0.0242, 0.6822,0.4049]';

%Here we define the accuracy for the first target. It is lower since, it will not affect
%the second trajectory:
acc1=1e-3;

%The variables to use inside the loop:
pE_curr=pE_0;
joints_curr=joints_0;

%Solving the forward Kinematic for the initial position to calculate Homogeneous Matrix.
g=RobotArm.fkine(joints_curr');
%We calculate the rotation matrices with accuracy of 4 decimal digits
R_0E=round(1e+4*[g.n,g.o,g.a])/1e+4;

%Calculate the error which will be used to solve IKine
error=[R_0E'*(pE_d1-pE_curr);zeros(3,1)];

% We store the Homogeneous Transformations and ThetaConfigurations to plot them afterwards.
% Also for plotting purposes we store qdot and velocity of the tool. Since we do not know
% the total no. of repetitions needed, these matrices will be dynamically stored. So, we
% can store the values of the first iteration.
global HomoRobotArm;
global Thetas;
global qdot;
global vtool;
HomoRobotArm(:,:,ctr)=[R_0E,transl(g)';zeros(1,3),1];
Thetas(ctr,:)=joints_curr';
qdot(ctr,:)=zeros(1,6);
vtool(:,ctr)=zeros(3,1);


while any(abs(error)>acc1)
   
   % Calculate Jacobian of the Tool (Je) at the current Joints' Configuration
   jacobian=RobotArm.jacobe(joints_curr');

   % Calculate the desired angular velocity of joints' angles based on error
   q_dot=((eye(6)/jacobian)*error);
   % This angular velocity at joints' angles can be translated in Linear (1:3) 
   % and Angular(4:6) Velocity of the Tool (since we are using Jacobian of Tool)
   V_tool=jacobian*q_dot;
   
   % The linear acceleration (the angular is zero) can be calculated as a=dv/dt
   l_acc=(V_tool(1:3)-vtool(:,ctr))/Ts;

   % Saturation of Linear Velocity to satisfy Velocity and Acceleration Constraints
   % Firstly we satisfy the acceleration constraint
   if norm(l_acc)>a_max
      
      % This is the maximum acceleration we can generate at each axis, so that the norm
      % is equal to the max allowed value
      l_acc=l_acc*a_max/norm(l_acc);
      
      % This is the new velocity that is generated with the maximum acceleration
      newV=vtool(:,ctr)+l_acc*Ts;  
      
      % We saturate qdot and V (only linear part since the other is zero)
      q_dot=q_dot*(norm(newV)/norm(V_tool(1:3)));
      V_tool(1:3)=newV;
      
   end
   %After we have satisfied the acceleration constraint we will ensure that the velocity
   %constraint is satisfied as well
   if norm(V_tool(1:3))>v_max
      %We just saturate the new values
      q_dot=q_dot*v_max/norm(V_tool(1:3));
      V_tool(1:3)=V_tool(1:3)*v_max/norm(V_tool(1:3));
   end
   % Euler Integration for joints angles
	joints_curr=joints_curr+Ts*q_dot;
	% For the next angles' configuration we will calculate the forward kinematics
   g=RobotArm.fkine(joints_curr);
   % The new rotation matrix
	R_0E=round(1e+4*[g.n,g.o,g.a])/1e+4;
   % The new position vector
	pE_curr=transl(g)';
   % Calculate the new error vector
   error=[R_0E'*(pE_d1-pE_curr);zeros(3,1)];
   
   % Move repetitions' counter forward
	ctr=ctr+1;
   
   %Save the values to be animated and plotted
	HomoRobotArm(:,:,ctr)=[R_0E,pE_curr;zeros(1,3),1];
	Thetas(ctr,:)=joints_curr';
	qdot(ctr,:)=q_dot;
	vtool(:,ctr)=V_tool(1:3);
   
end

%This is the second target and it is exactly the top middle point of the cylinder.
%The accuracy here is higher since it will affect the next trajectory
pE_d2=[-0.0242, 0.6822,0.1]';
acc2=1e-3;

%pE_curr and joints_curr remain from the previous loop


%Calculate the error which will be used to solve IKine
error=[R_0E'*(pE_d2-pE_curr);zeros(3,1)];

while any(abs(error)>acc2)
   
   % Calculate Jacobian of the Tool (Je) at the current Joints' Configuration
   jacobian=RobotArm.jacobe(joints_curr');

   % Calculate the desired angular velocity of joints' angles based on error
   q_dot=((eye(6)/jacobian)*error);
   % This angular velocity at joints' angles can be translated in Linear (1:3) 
   % and Angular(4:6) Velocity of the Tool (since we are using Jacobian of Tool)
   V_tool=jacobian*q_dot;
   
   % The linear acceleration (the angular is zero) can be calculated as a=dv/dt
   l_acc=(V_tool(1:3)-vtool(:,ctr))/Ts;

   % Saturation of Linear Velocity to satisfy Velocity and Acceleration Constraints
   % Firstly we satisfy the acceleration constraint
   if norm(l_acc)>a_max
      
      % This is the maximum acceleration we can generate at each axis, so that the norm
      % is equal to the max allowed value
      l_acc=l_acc*a_max/norm(l_acc);
      
      % This is the new velocity that is generated with the maximum acceleration
      newV=vtool(:,ctr)+l_acc*Ts;  
      
      % We saturate qdot and V (only linear part since the other is zero)
      q_dot=q_dot*(norm(newV)/norm(V_tool(1:3)));
      V_tool(1:3)=newV;
      
   end
   %After we have satisfied the acceleration constraint we will ensure that the velocity
   %constraint is satisfied as well
   if norm(V_tool(1:3))>v_max
      %We just saturate the new values
      q_dot=q_dot*v_max/norm(V_tool(1:3));
      V_tool(1:3)=V_tool(1:3)*v_max/norm(V_tool(1:3));
   end
   % Euler Integration for joints angles
	joints_curr=joints_curr+Ts*q_dot;
	% For the next angles' configuration we will calculate the forward kinematics
   g=RobotArm.fkine(joints_curr);
   % The new rotation matrix
	R_0E=round(1e+4*[g.n,g.o,g.a])/1e+4;
   % The new position vector
	pE_curr=transl(g)';
   % Calculate the new error vector
   error=[R_0E'*(pE_d2-pE_curr);zeros(3,1)];
   
   % Move repetitions' counter forward
	ctr=ctr+1;
   
   %Save the values to be animated and plotted
	HomoRobotArm(:,:,ctr)=[R_0E,pE_curr;zeros(1,3),1];
	Thetas(ctr,:)=joints_curr';
	qdot(ctr,:)=q_dot;
	vtool(:,ctr)=V_tool(1:3);
end

%At this point the Tool has picked the cylinder, so we need to store this repetition
%in order for the cylinder to start moving as well.
 startCylinder=ctr;
%Now the trajectory is not defined by a final position, but we will use the predefined
%trajectory for Velocity designed in previous questions:
for k=1:length(positionE(1,:))
      % calculate forward Kinematics
      jacobian=RobotArm.jacobe(joints_curr');  
      % In order to follow the desired Velocity Trajectory
      q_dot=(eye(6)/jacobian)*[velocityE(:,k);zeros(3,1)];
      % Euler Integration
      joints_curr=joints_curr+q_dot*Ts;
      % Forward Kinematics for new orientation and position
      g=RobotArm.fkine(joints_curr);
      R_0E=round(1e+4*[g.n,g.o,g.a])/1e+4;
      pE_curr=transl(g)';
      % Move repetitions forward
      ctr=ctr+1;
      %Save variables
      HomoRobotArm(:,:,ctr)=[R_0E,pE_curr;zeros(1,3),1];
      Thetas(ctr,:)=joints_curr';
      qdot(ctr,:)=q_dot;
      vtool(:,ctr)=velocityE(:,k);
end

% Calculate the acceleration used during the whole movement: a=dv/dt
atool=zeros(3,ctr-1);
normatool=zeros(1,ctr-1);
for k=2:ctr     
      atool(:,k-1)=(vtool(:,k)-vtool(:,k-1))/Ts;
      normatool(k-1)=norm(atool(:,k-1));
end

% Total Data to be plotted
%The position of the tool during the movement based on the stored Homogeneous Matrices
ptool=transl(HomoRobotArm)';

%The orientation of the tool during the movement based on the stored Homogeneous Matrices
quats=rotm2quat(HomoRobotArm(1:3,1:3,:))';

%% Animation
animatePandP(startCylinder,pE_2)

%%
clear acc1 acc2 ctr joints_curr l_acc newV normatool atool g error q_dot R_0E R_E0...
      startCylinder T_E_0 T_E_1 T_E_final tspan_desired tspan_init V_tool vtool normvtool;
