function[]=animatePandP(startCylinder,pB)

% This function animates the movement of the cylinder
global joints_0;
global RobotArm;
global Thetas;
global HomoRobotArm;


i=length(Thetas(:,1));
% Position, orientation and Homegeneous Transformation of frames A and B
pA=[-0.0242,0.6822,0]';    %(3x1)
qA=[0.9588,0,0,0.2840];    %(1x4)
TA=[quat2rotm(quaternion(qA)),pA;zeros(1,3),1];    %(4x4)
   
qB=[0,0,0,1];                 %(1x4)
TB=[quat2rotm(quaternion(qB)),pB;zeros(1,3),1];    %(4x4)

% Create the cylinder for frame A
[X,Y,Z] = cylinder(0.025,100);
Z=Z*0.1;
% %Plot the environment with the RobotArm at the initial position-orientation

figure()
      set(gcf,'Visible','on')
      %Plot the outside box in which everything will be included
      plotcube([3 3 3],[ -1.5 -1.5 -1.5],0,[0 0 0]);
      hold on
      h=mesh(X+pB(1),Y+pB(2),Z+pB(3)-0.1);
      set(h,'EdgeColor','#A2142F'	,'FaceColor','#A2142F'	)
      %Plot the cylinder in the right location and with the right scaling, h=10cm (z-axis)
      h=mesh(X+pA(1),Y+pA(2),(Z+pA(3)));
      h0=mesh(X+pA(1),Y+pA(2),(Z+pA(3)));
      set(h,'EdgeColor','#EDB120'	,'FaceColor','#EDB120'	)
      set(h0,'EdgeColor','#EDB120'	,'FaceColor','#EDB120'	)

      %Plot the general table (Frames {0} and {A})
      plotcube([0.6 1.2 0.001],[ -0.1 0 -0.001],1,[222/255 184/255 135/255]);
      
      %Plot the table to work on (Frame {B})
      plotcube([+0.2 +0.6 +0.08],[ +0.3 +0.3 0],1,[255/255 229/255 204/255]);
      
      % Plot the frames {0},{A} and {B}
      %H0=trplot(eye(4),'frame','0','rgb','length',0.05,'text_opts',{'FontSize', 10});
      %HA=trplot(TA,'frame','A','rgb','length',0.05,'text_opts',{'FontSize', 10});
      %HB=trplot(TB,'frame','B','rgb','length',0.05,'text_opts',{'FontSize', 10});
      %HA0=trplot(TA,'frame','A','rgb','length',0.05,'text_opts',{'FontSize', 10});

      %Plot the RobotArm in its initial position
      RobotArm.plot(joints_0');
      
for counter=1:20:i
   
   RobotArm.animate(Thetas(counter,:))
   
   if counter>=startCylinder
      pCylinder=transl(HomoRobotArm(:,:,counter))-[0;0;0.1];
      delete(h)
      h=mesh(X+pCylinder(1),Y+pCylinder(2),(Z+pCylinder(3)));
      set(h,'EdgeColor','#4DBEEE'	,'FaceColor','#4DBEEE'	)
      % Update frame plot
      %TA=[quat2rotm(quaternion(qA)),pCylinder;zeros(1,3),1];    %(4x4)
      %delete(HA)
      %HA=trplot(TA,'frame','A','rgb','length',0.05,'text_opts',{'FontSize', 10});
   end
end

h=mesh(X+pCylinder(1),Y+pCylinder(2),Z+pCylinder(3));
set(h,'EdgeColor','#EDB120','FaceColor','#EDB120')

end
