function []=plot_3d(X,Y,Z,size,midpoint)

scatter3(X(2:midpoint),Y(2:midpoint),Z(2:midpoint),size*ones(1,midpoint-1),...
   ones(midpoint-1,1)*[1,0,0],'MarkerFaceColor',[1,0,0]);
grid on
hold on
scatter3([X(1);X(midpoint);X(end)],[Y(1);Y(midpoint);Y(end)],...
   [Z(1);Z(midpoint);Z(end)],3*size,'*k','LineWidth',2)
scatter3(X(midpoint:end-1),Y(midpoint:end-1),Z(midpoint:end-1),size*ones(1,length(X)-...
   midpoint),ones(length(X)-midpoint,1)*[0,0.5,0],'MarkerFaceColor',[0,0.5,0]);
end

