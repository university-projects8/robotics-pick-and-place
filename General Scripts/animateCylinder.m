function animateCylinder(traj)

   qA=[0,0,0,1];
   qB=[0,0,0,1];
   pA=[-0.0242,0.6822,0]';
   pB=[0.3884,0.5967, 0.08]';
   
   if traj==1  
      Traj=load('trajectory3rdOrder.mat');
   else
      Traj=load('trajectoryLinParab.mat');
   end
   
   pCylinder=Traj.s_total(:,1:4000:end);
   pCylinder=[pCylinder,pCylinder(:,end)];
   TA=[quat2rotm(quaternion(qA)),pA;zeros(1,3),1];    %(4x4)

   TB=[quat2rotm(quaternion(qB)),pB;zeros(1,3),1];    %(4x4)
   Tseq=zeros(4,4,size(pCylinder,2));
   
   for i=1:size(pCylinder,2)
      Tseq(:,:,i)=[quat2rotm(quaternion(qA)),pCylinder(:,i);zeros(1,3),1];
   end
   % Create the cylinder for frame A
   [X,Y,Z] = cylinder(0.025,100);
   Z=Z*0.1;
   % %Plot the environment with the RobotArm at the initial position-orientation
   figure()
         set(gcf,'Visible','on')

         %Plot the outside box in which everything will be included
         plotcube([3 3 3],[ -1.5 -1.5 -1.5],0,[0 0 0]);
         hold on

         h0=mesh(X+pCylinder(1,1),Y+pCylinder(2,1),(Z+pCylinder(3,1)));
         set(h0,'EdgeColor','#A2142F','FaceColor','#A2142F')
         h=mesh(X+pCylinder(1,1),Y+pCylinder(2,1),(Z+pCylinder(3,1)));
         set(h,'EdgeColor','#A2142F','FaceColor','#A2142F')      

         %Plot the general table (Frames {0} and {A})
         plotcube([0.6 1.2 0.001],[ -0.1 0 -0.001],1,[222/255 184/255 135/255]);

         %Plot the table to work on (Frame {B})
         plotcube([+0.2 +0.6 +0.08],[ +0.3 +0.3 0],1,[255/255 229/255 204/255]);

         %Plot the frames {0},{A} and {B}
         H0=trplot(eye(4),'frame','0','rgb','thick',1.2,'length',0.2,'text_opts', {'FontSize', 12});

         HA=trplot(TA,'frame','A','rgb','thick',1.2,'length',0.2,'text_opts', {'FontSize', 12});
         HA0=trplot(TA,'frame','A','rgb','thick',1.2,'length',0.2,'text_opts', {'FontSize', 12});
         %HB=trplot(TB,'frame','B','rgb','thick',1.2,'length',0.2,'text_opts', {'FontSize', 12});
         HB=mesh(X+pCylinder(1,end),Y+pCylinder(2,end),(Z+pCylinder(3,end)));
         set(HB,'EdgeColor','#A2142F','FaceColor','#A2142F','FaceAlpha',0.5)
         
         for i=1:5:size(pCylinder,2)
            delete(HA)
            HA=trplot(Tseq(:,:,i),'rgb','thick',1.2,'length',0.2,'text_opts', {'FontSize', 12});
            delete(h)
            h=mesh(X+pCylinder(1,i),Y+pCylinder(2,i),(Z+pCylinder(3,i)));
            set(h,'EdgeColor','k','FaceColor','k')
            pause(0.1)
         end
         h=mesh(X+pCylinder(1,end),Y+pCylinder(2,end),(Z+pCylinder(3,end)));
         set(h,'EdgeColor','#A2142F','FaceColor','#A2142F')
         
end
