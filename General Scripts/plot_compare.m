function [] = plot_compare(grid_on,x_vector,y_vectors1,y_vectors2,limits,x_label,y_label,...
                     legends,name,xtick_step,xPoints,oPoints,ooPoints)

xtick=x_vector(1):xtick_step:x_vector(end);
len=size(x_vector,2);
nY1=size(y_vectors1,1);
nY2=size(y_vectors2,1);
nL=size(limits,1);
nX=size(xPoints,1);
nO=size(oPoints,1);
nOO=size(ooPoints,1);
if grid_on==1
   grid on
else
   grid off
end
hold on
colors=[0, 0.4470, 0.7410;0.9290, 0.6940, 0.1250;0.6350, 0.0780, 0.1840;...
         0.3010, 0.7450, 0.9330;0 , 0, 0;1, 0, 0];
for i=1:nY1
    plot(x_vector,y_vectors1(i,:),'-','LineWidth',2,'Color',colors(i,:))
end

for i=1:nY2
    plot(x_vector,y_vectors2(i,:),'--','LineWidth',2,'Color',colors(nY1+nY2+1-i,:))
end

for i=1:nL
   plot(x_vector,limits(i)*ones(1,len),'--','LineWidth',2,'Color',[0.75, 0, 0.75])
end

for i=1:nX
   plot(xPoints(i,1),xPoints(i,2),'x',...
      'MarkerSize', 8,'Color',[1 0 0],'LineWidth',1)
end

for i=1:nO
   plot(oPoints(i,1),oPoints(i,2),'o','Color',[0 0.5 0],...
      'MarkerSize', 5,'MarkerFaceColor',[0, 0.5, 0])
end

for i=1:nOO
   plot(ooPoints(i,1),ooPoints(i,2),'ok','MarkerSize', 8,'LineWidth',1)
end
if x_vector(end)>xtick(end)
   xtick=[xtick,round(x_vector(end)*100)/100];
end

xticks(xtick)
xlabel(x_label)
ylabel(y_label)
title(name)
legend(legends,'Location','best')
hold off
end


