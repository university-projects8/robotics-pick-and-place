# Robotics Pick and Place

The university project of the Robotics course on the summer semester of the academic year 2019-2020. It consists of three separate parts focusing on trajectory design, robot control and DMP training respectively.

### Part A

Consider a pick-and-place scenario of an object. The inertial frame {0} is located on a horizontal workbench. A cylinder-shaped object (10 cm height and 2.5 cm radius) is also located on the same workbench. Its frame, {A}, is pinpointed at the center of the object's base. We want to move the object to the point where frame {B} is located, which is at a raised (by 8 cms) subspace of the workbench. The orientation of the coinciding frames can be anyone, only the origins of the frames must overlap.

The pose and orientation (Quaternion) of frames {A} and {B} is given with respect to {0}, as well as the location of the raised subspace (cuboid) of the workbench:

<img src="https://latex.codecogs.com/gif.latex?p_{0A} = \begin{bmatrix} -0.0242 \\ 0.6822 \\ 0 \end{bmatrix},  \; p_{0B} = \begin{bmatrix} 0.3884 \\ 0.5967 \\ 0.08 \end{bmatrix}, \; Q_{0A} = \begin{bmatrix} 0.9588 \\ 0 \\ 0 \\ 0.2840\end{bmatrix},  \; Q_{0B} = \begin{bmatrix} 0 \\ 0 \\ 0 \\ 1\end{bmatrix}" />

The cuboid range per axis (with respect to {0}):

<img src="https://latex.codecogs.com/gif.latex?\begin{bmatrix} x \\ y\\ z \end{bmatrix} = \begin{bmatrix} 0.3 - 0.80 \\ 0.3 - 0.80 \\ 0.0 - 0.08 \end{bmatrix} " />

- A1) Calculate a position and velocity trajectory for the object in order to move from the frame's {A} origin to the frame's {B} origin without colliding with its environment. The trajectory's duration must be 3 sec, while the limit values for velocity and acceleration are 0.35 m/sec and 0.7 m/sec^2 respectively. The trajectory must be calculated using 2 different methodologies: a) 3rd-order polynomial and b) linear trajectory with parabolic blend.

- A2) Simulate the trajectories for the object in 3D-space.

### Part B

Consider a 6-DOF robot with 6 revolute joints. The base of the robot coinsides with the inertial frame {0}, while the tool of the robot is represented with the frame {E}. The robot is controlled using velocity commands in the joints' space <img src="https://latex.codecogs.com/gif.latex?\dot{\boldsymbol{q}} " />. The initial values for each joint, as well as the position and orientation of the tool {E} are the following:

<img src="https://latex.codecogs.com/gif.latex?q_{init} = \begin{bmatrix} 2.1595  & -0.6995 & 1.2719 & 3.1416 & 1.2002 & -0.9821 \end{bmatrix}^{T}," />  

<img src="https://latex.codecogs.com/gif.latex?p_{0E} = \begin{bmatrix} -0.3397 \\ 0.5088 \\ 0.4049 \end{bmatrix}, \; Q_{0E} = \begin{bmatrix} 0 \\ 0 \\ 1 \\0\end{bmatrix}" />

- B1) We want to control the robot in order to pick the object from the center of its top base and then transport it to the desired position, frame's {B} origin, while keeping the orientation of the object *constant* during the whole movement. The model of the robot was provided among the project announcement with the `lwr_create.p` file, which is called as `lwr = lwr_create()`.

- B2) Simulate the robot movement in a 3D-virtual environment.

### Part C

Suppose that the above task (part B) is now kinaesthetically demonstrated to the robot by a user. The position of the tool is recorded during the demonstration with a frequency of 500 Hz. The data are provided among the project announcement in the files `trajectory_picκ_ΕΑ.dat` (3xK1) and `trajectory_place_ΑΒ.dat` (3xK2).

- C1) Train two DMPs (one for each part of the movement) using the learn_weights.p file provided among the project announcement.  
As far as `learn_weights.p` file is concerned, it is called as: `[c,h,w,ax] = learn_weights(fd,y0,g,tau,total_time,N,dt)` where: c is the vector containing the centers of the Gaussians (1xN), h is the inverse amplitude of the Gaussians (1xN), w is the weights of the Gaussians (3xN), ax is the parameter of the canonical system fd is the desired guiding term for each timestamp (3xK), y0 is the initial position (3x1), g is the goal position (3x1), tau: is the time escalation parameter, total_time is the duration of the movement, N is the number of Gaussian Base Functions, dt is the control period and K is the total number of timestamps.

- C2) Simulate the movement as a result of the trained DMPs and compare the tool's position to the data provided.

- C3) Consider that this cylinder must be placed in another position, namely in: 
<img src="https://latex.codecogs.com/gif.latex?p_{0B2} = \begin{bmatrix} 0.5 \\ 0.45 \\ 0.08 \end{bmatrix}" />, while the tool is located on the same initial position. Simulate the movement using the already trained DMPs and compare it to C2.

## Simulation Results

#### Part A: Trajectory Design

Firstly, two simulations of the 3rd-order polynomial trajectory:

<p float=" left ">
  <img src="/Part A/Sims/Cylinder1.gif" width="32%" />
  <img src="/Part A/Sims/Cylinder2.gif" width="34%" />
  <img src="/Part A/Sims/Cylinder3.gif" width="32.5%" />
</p>

Followingly, two simulations of the Linear trajectory with parabolic blend:

<p float=" left ">
  <img src="/Part A/Sims/Parabolic1.gif" width="32%" />
  <img src="/Part A/Sims/Parabolic2.gif" width="34%" />
  <img src="/Part A/Sims/Parabolic3.gif" width="32.5%" />
</p>

#### Part B: Robot Control
The robot picks and place the object controlled on its joint's velocities. The simulation mildly accelerated (x1.667) from 3 different views:  

<p align=" center ">
  <img src="/Part B/Sims/B_sim_1.gif" width="50%" />
</p>

<p align="center">
  <img src="/Part B/Sims/B_sim_2.gif" width="50%" />
</p>

<p align="center">
  <img src="Part B/Sims/B_sim_3.gif" width="50%" />
</p>

<p align="center">
  <img src="Part B/Sims/B_sim_1.gif" width="33%" />
  <img src="Part B/Sims/B_sim_2.gif" width="33%" />
  <img src="Part B/Sims/B_sim_3.gif" width="33%" />
</p>

#### Part C: DMPs
Simulations of the robot movement using the trained DMPs. The first 3 simulations use as a goal position the
same one as the kinaesthetic demonstration:

<p align=" center ">
  <img src="/Part C/Sims/C_Sim_1.gif" width="50%" />
</p>

<p align="center">
  <img src="/Part C/Sims/C_Sim_2.gif" width="50%" />
</p>

<p align="center">
  <img src="Part C/Sims/C_Sim_3.gif" width="50%" />
</p>

<p align="center">
  <img src="Part C/Sims/C_Sim_1.gif" width="34%" />
  <img src="Part C/Sims/C_Sim_2.gif" width="31%" />
  <img src="Part C/Sims/C_Sim_3.gif" width="34%" />
</p>

Followingly, the goal target is changed and the robot adapts.

<p align=" center ">
  <img src="/Part C/Sims/C_Sim_4.gif" width="50%" />
</p>

<p align="center">
  <img src="/Part C/Sims/C_Sim_6.gif" width="50%" />
</p>

<p align="center">
  <img src="Part C/Sims/C_Sim_5.gif" width="50%" />
</p>

<p align="center">
  <img src="Part C/Sims/C_Sim_4.gif" width="34%" />
  <img src="Part C/Sims/C_Sim_6.gif" width="31%" />
  <img src="Part C/Sims/C_Sim_5.gif" width="34%" />
</p>


## Project Files
    --------------------------------------------General Functions:--------------------------------------------
	---animateCylinder.m
		Inputs: 
			traj: a variable indicating which trajectory for
				the cylinder should be animated
		Outputs: ()
		Function: animates a trajectory for the cylinder designed
				in Part_A.
				
	---animatePandP.m
		Inputs:
			startCylinder: a variable indicating when the robot has
				picked the cylinder, so that it can start moving as
				well
			pB:	the final position of the cylinder
		Outputs: ()
		Function: animates a trajectory for the pick and place trajectory.
	
	---plot_2d.m
		Inputs:
			grid_on: a variable indicating whether grid should be added in
				the figure or not. (1==grid on, 0==grid off)
			x_vector: the vector to be plotted in x_axis. It is either the
				running time of a simulation or different values of a timepoint
				during sensitivity analysis. (1xM)
			y_vectors: the vectors to be plotted in y_axis. (NxM)
			limits: a number indicating a static limit for a value (scalar)
			x_label: the label for x_axis. (string)
			y_label: the label for y_axis. (string)
			legends: the legends of the plot(array of cells containing strings)
			name: title of the plot (string)
			xtick_step: a number indicating the difference between xticks (scalar)
			xPoints: points in the figure to be plotted with an X (Nx2)
			oPoints: points in the figure to be plotted with an o (Nx2)
			ooPoints: points in the figure to be plotted with a double o (Nx2)
		Outputs: ()
		Function: plots 2-d diagrams in a suitable form. For subplots, it should
			be called for each subplot.
			
	---plot_3d.m
		Inputs:
			X: A vector containing the X-positions of the trajectory. (1xM)
			Y: A vector containing the X-positions of the trajectory. (1xM)
			Z: A vector containing the X-positions of the trajectory. (1xM)
			size: The size of the dots to be plotted, as scalar. (Matlab default =36)
			midpoint: The point where the Cylinder will be picked.
		Output: ()
		Function: it generates a 3d-Figure of the trajectory of the robot and the 
			cylinder. At the points of interest (initial position of Robot, initial
			position of Cylinder and final position of both) there are some snowflakes
			generated.
		
	---plot_compare.m
		Inputs: almost the same with plot_2d. The only difference is that
			instead of y_vectors, we have y_vectors1 and y_vectors2, which are
			groups of vectors which we want to compare 1 by 1.
		Output: ()
		Function: the same with plot_2d
	---plotcube.m
	"Olivier (2020). PLOTCUBE (https://www.mathworks.com/matlabcentral
	/fileexchange/15161-plotcube), MATLAB Central File Exchange.
	Retrieved May 24, 2020."
	
	
    ----------------------------------------------------Part_A:----------------------------------------------------
	At this part there are 2 different trajectories for the cylinder generated. These
	are aninmated in Matlab. The associated files are:
	
	Associated Files:
		parameters_gen.m:
			Function:
			This file handles symbolic variables to calculate the parameters of a 3rd-order
			polynomial trajectory in terms of the initial, intermediate and final positions
			and the duration of each part of the trajectory (tmed and t_total).
			This is used in order to calculate the specific parameters for different tmed values.
		parameters.m:
			Inputs:
			---qA: initial position of Cylinder (3x1)
			---qB: final position of Cylinder(3x1)
			---qM: intermediate position of Cylinder (in order to avoid collision)(3x1)
			---tf:	total duration of movement (scalar)
			Function:
			It calculates the parameters based on parameters_gen for specific positions and
			total time, only varying tmed. It returns the acceleration values in t=0,tmed,tf
			for each tmed value ranging in [0.5, tf-0.5] among the maximum velocity component
			per axis for each tmed value.
			Output: 
			---tau: vector of tmed values	(1xM)
			---a1: acceleration vector in t=0 per tmed value	(3xM)
			---a2: acceleration vector in t=tmed per tmed value	(3xM)
			---a3: acceleration vector in t=tf per tmed value	(3xM)
			---V_max: maximum velocity per axis for each tmed value (3xM)
		eval_time.m:
			Inputs:
			---x1: input vector to be evaluated (1xN)
			---x2: input vector to be evaluated (1xN)
			---x3: input vector to be evaluated (1xN)
			---t:	vector whose values will be selected as accepted/rejected (1xN)
			---limit: a positive quantity restraining the input vectors (scalar)
			Function:
			Evaluates if ALL the components of x1,x2,x3 satisfy the constraint abs(x)<=lim,
			x=[x1;x2;x3].If so the respective value of t vector is considered accepted, 
			otherwise rejected.
			Output:
			---accept: a vector containing the values of t vector which are considered accepted (1xM)
			---reject: a vector containing the values of t vector which are considered rejected (1xM)
		main_A_1.m:
			Function:
			This script generates the 3rd-Order Polynomial Trajectory using trajpolynomial.m.
			Firstly, it calls parameters.m and evaluates the returned acceleration and velocity
			vectors. The results of the evaluation are plotted in a separate section (Section 5).
			Section 6 containst the actual simulation after having selected a value for tmed, its
			animation is generated in Section 8 and the respective plots of the trajectory are generated 
			in Section 7. The trajectory is	saved in a .mat file (position, velocity and acceleration)
			in Section 9.
		A_1_plots.m:
			Function:
			This script is a part of main_A_1.m which only generates the diagrams of the trajectory.
		A_1_animation.m:
			Function:
			This script is a part of main_A_1.m which only generates the animation of the trajectory.
		trajPolynomial.m:
			Inputs: same with parameters.m file but also takes as input a specific tmed value
			Function: 
			It is the same with parameters.m file, but here since we have a specific
			trajectory to generate it returns the specifci trajectory having calculated its parameters.
			Outputs:
			---tspan:	vector of time during the simulation (1xM)
			---s_tot:	trajectory for position (3xM)
			---v_tot:	trajectory for velocity (3xM)
			---a_tot:	trajectory for acceleration (3xM)
		main_A_2.m:
			Function:
			This script file generates a linear trajectory with parabolic blends. The design process
			follows J. Craig: Introduction to Robotics example 7.3 (Third Edition). In Section 3 the
			design process is implemented and in Sections 4 and 5 the trajectory (position,
			velocity and acceleration) is generated and animated respectively. 
			Section 6 contains the diagrans for this trajectory	and the trajectory is saved in a .mat 
			file in Section 7.
		A_2_plots.m:
			Function:
			This script is a part of main_A_2.m which only generates the diagrams of the trajectory.
		A_2_animation.m:
			Function:
			This script is a part of main_A_2.m which only generates the animation of the trajectory.
    ----------------------------------------------------Part_B----------------------------------------------------
	At this part the Robot picks the Cylinder from its initial position and places it
	at the desired one following a trajectory from Part_A (for the "place" part), while
	maintaining its orientation constant during the whole movement.

	Associated Files:
		main_B_1.m:
			Function:
			It solves the Inverse Kinematics problem. It calculates the qdot values needed to control the Robot
			so that it can pick the cylinder and then following the inserted trajectory, to place it in the final
			position. The inserted trajectory is the 3rd Order Polynomial Trajectory from A_1.
		B_1_plots.m:
			Function:
			This script is a part of main_A_2.m which only generates the diagrams of the trajectory.
		B_1_animation.m:
			Function:
			This script is a part of main_A_2.m which only generates the animation of the trajectory.
		main_B_2.m:
			Function:
			It solves the Inverse Kinematics problem. It calculates the qdot values needed to control the Robot
			so that it can pick the cylinder and then following the inserted trajectory, to place it in the final
			position. The inserted trajectory is the Linear Trajectory with Parabolic Blends from A_2.
		B_2_plots.m:
			Function:
			This script is a part of main_B_2.m which only generates the diagrams of the trajectory.
		B_2_animation.m:
			Function:
			This script is a part of main_B_2.m which only generates the animation of the trajectory.		
		
    -----------------------------------------------------Part_C-----------------------------------------------------
	At this part 2 different Dynamic Movement Primitives are trained based on provided
	trajectory data for the "pick" and the "place" part. The movement of the trained Robot
	is simulated and afterwards it tries to place the object in a different location. These
	two different movements are animated and compared between each other and between the
	original training data.
	
	Associated Files:
		main_C.m:
			Function:
			This script firstly reads the input data of the trajectories (Section 1) and secondly sets some
			constant parameters for the learning process (Section 2). Sections 3 and 4 train a distinctive
			DMP for each part of the trajectory (pick and place) and Section 5 simulates the movement based 
			on the DMP executions of 3 and 4. Section 6 creates the animation of the trajectory and Section 7
			creates the diagrams of the movement.
		C_1_plots.m:
			Function:
			This script is a part of main_B_2.m which only generates the diagrams of the trajectory.
		C_1_animation.m:
			Function:
			This script is a part of main_B_2.m which only generates the animation of the trajectory.
		main_C_2.m:
			Function: 
			It is the same as main_B.m, but just before execution of the 2nd DMP (after its training)
			the goal position is set to a different one.
		C_2_plots.m:
			Function:
			This script is a part of main_B_2.m which only generates the diagrams of the trajectory.
		C_2_animation.m:
			Function:
			This script is a part of main_B_2.m which only generates the animation of the trajectory.	
					
    ------------------------------------------How to execute each part:--------------------------------------------

    Part_A: files main_A_1.m and main_A_2.m generate the respective trajectories and the diagrams and the animation.
	  In order to generate only the diagrams of the trajectory, run A_1_plots.m or A_2_plots.m respectively.
	  In order to generate only the diagrams of the trajectory, run A_1_animation.m or A_2_animation.m respectively.
    Part_B: files main_B_1.m and main_B_2.m solve the IK Problem for each trajectory from part_A among the diagrams and
	  the animation.
	  In order to generate only the diagrams of the movement, run B_1_plots.m or B_2_plots.m respectively.
	  In order to generate only the diagrams of the movement, run B_1_animation.m or B_2_animation.m respectively.
    Part_C: files main_C_1.m and main_C_2.m train two DMPs for the two parts of trajectory, for two different goal positions.
	  In order to generate only the diagrams of the movement, run C_1_plots.m or C_2_plots.m respectively.
	  In order to generate only the diagrams of the movement, run C_1_animation.m or C_2_animation.m respectively.
	
    ---------------------------------------------------ANIMATION---------------------------------------------------

	The animations have an initial pause of 5-10 seconds in  order to adjust the view angle as soon as the figure()
	object appears on screen. The black cylinders at the initial and final position indicate the start and the end
	of the movement.
	
    -----------------------------------------------------PLOTS-----------------------------------------------------

    Part_A_1: Contains 12 Figures
	---Acceleration sensitivity plots (x3, at the initial, intermediate and final moments).
	---Velocity sensitivity plot (x1, maximum of the velocity norm).
	---Position plots for tmed=2.1 sec (x2, one total and one with subplots).
	---Velocity plots for tmed=2.1 sec (x2, one total and one with subplots).
	---Acceleration plots for tmed=2.1 sec (x2, one total and one with subplots).
	
	The first four of them are plotted separately in tmed_selection.m and the rest in A_1_plots.m
	
    Part_A_2: Contains 8 Figures 

	---Position plots (x2, one total and one with subplots).
	---Velocity plots (x2, one total and one with subplots).
	---Acceleration plots (x2, one total and one with subplots).
	
    Part_B: (Each part) Contains 19 Figures

	---3d-Trajectory for Position (x3, separately with and without the integrated environment)
	---Position plots of Tool (x2, one total and one with subplots).
	---Orientation plots of Tool (x2, one total and one with subplots).
	---Joints Angles plots in rad (x2, one total and one with subplots).
	---Joints Angles plots in deg (x2, one total and one with subplots).
	---Joints Velocity plots in rad/sec (x2, one total and one with subplots).
	---Velocity plots of Tool (x2, one total and one with subplots).
	---Velocity Norm plot (x1).
	---Acceleration plots of Tool (x2, one total and one with subplots).
	---Acceleration Norm plot (x1).

    Part_C: (Each part) Contains 18 Figures

	---3d-Trajectory for Position (x3, separately with and without the integrated environment)
	---Position plots of Tool (x2, one total and one with subplots).
	---Orientation plots of Tool (x2, one total and one with subplots).
	---Joints Angles plots in rad (x2, one total and one with subplots).
	---Joints Angles plots in deg (x2, one total and one with subplots).
	---Joints Velocity plots in rad/sec (x2, one total and one with subplots).
	---Velocity plots of Tool (x2, one total and one with subplots).
	---Velocity Norm plot (x1).
	---Comparison plots of Tool (x2, one total and one with subplots).
			--- for C_1 we compare the desired trajectory with the DMP actual one.
			--- for C_2 we compare the DMP actual trajectory with a different goal than the
				training and the same goal with the training.
