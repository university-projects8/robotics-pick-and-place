
%% Initializations

% Velocity and Trajectory Limitations
limit_V=0.35/sqrt(3);   limit_A=0.7/sqrt(3);

Ts=1e-6;    % Timestep

% Initial, Middle1, Middle2 and Final Positions
q1=[-0.0242;0.6822;0]; q2=[0.25;0.62;0.08]; q3=[0.30;0.61;0.10]; q4=[0.3884;0.5967;0.08];

% Initial Velocity
v0=zeros(3,1);

%% Trajectory Parameters Design based on J. Craig

%1st SEGMENT
%
%
%We design a time duration for this part (1-2)
td12=1.75;

%We will find the max Dq so as to assign there the max acceleration:
dq=q2-q1; 
[~,max_pos]=max(abs(dq));

% Dq is max on X-axis, so:
a1=zeros(3,1); a1(max_pos)=sign(dq(max_pos))*limit_A;

% We can calculate based on that Dq, tb1
tb1=td12-sqrt(td12^2-2*(dq(max_pos))/a1(max_pos));

% Since tb1 is known, max velocities and accelerations now can be calculated
v12=dq/(td12-0.5*tb1);
a1=v12/tb1;
   
%2nd SEGMENT
%
%
%We design a time duration for this part (2-3)
td23=0.45;

%We will find the Dq's so as to eventually find max Dv in order to assign max acceleration:
dq=q3-q2; 
v23=dq/td23;

%Find max DV
dv=v23-v12;
[~,max_pos]=max(abs(dv));

% Dv is max on X-axis, so:
a2=zeros(3,1); a2(max_pos)=sign(dv(max_pos))*limit_A;

% We can calculate based on that Dv, tb2
tb2=dv(max_pos)/a2(max_pos);

% Now we can find the linear part's duration
tl12=td12-tb1-tb2/2;

% And also the acceleration for the second parabolic part
a2=(dv)/tb2;

%Last SEGMENT
%
%
%We design a time duration for this part (2-3)
td34=3-td12-td23;

%We will find the Dq's so as to find max in order to assign max acceleration at that axis:
dq=q4-q3; 
[~,max_pos]=max(abs(dq));

%Again the maximum is at x-axis:
a4=zeros(3,1); a4(max_pos)=-sign(dq(max_pos))*limit_A;

% We can calculate based on that Dq, tb4
tb4=td34-sqrt(td34^2+2*dq(max_pos)/a4(max_pos));

% Now velocities and accelerations for the last segment can be designed
v34=dq/(td34-0.5*tb4);
a4=-v34/tb4;

%Pre-Last SEGMENT:
%
%
%Find max of DVs
dv=v34-v23;
[~,max_pos]=max(abs(dv));

%Max is Z, so we assign max acceleration at z
a3=zeros(3,1); a3(max_pos)=sign(dv(max_pos))*limit_A;

% Now we can calculate tb3 and then find the other accelerations
tb3=(dv(max_pos))/a3(max_pos);
a3=dv/tb3;

%Linear Segments Duration can be calculated since we found tb3
tl23=td23-0.5*tb2-0.5*tb3;
tl34=td34-tb4-0.5*tb3;

%% Trajectory Equations of Motion (Position,Velocity, Acceleration)
%1st Parabolic Blend
t1=0:Ts:tb1;
tspan1=t1;  
s1=q1+v0.*t1+0.5*a1.*t1.^2;
v1=v0+a1.*t1;
a1=a1*ones(1,size(tspan1,2));

%1st Linear Part
t12=0:Ts:tl12;
tspan2=tspan1(end)+t12(2:end);
s12=s1(:,end)+v12(:,end).*t12;
v12=v1(:,end)*ones(1,size(t12,2));
a12=zeros(3,size(tspan2,2));

%2nd Parabolic Blend
t2=0:Ts:tb2;
tspan3=tspan2(end)+t2(2:end);
s2=s12(:,end)+v12(:,end).*t2+0.5*a2.*t2.^2;
v2=v12(:,end)+a2.*t2;
a2=a2*ones(1,size(tspan3,2));

%2nd Linear Part
t23=0:Ts:tl23;
tspan4=tspan3(end)+t23(2:end);
s23=s2(:,end)+v23(:,end).*t23;
v23=v2(:,end)*ones(1,size(t23,2));
a23=zeros(3,size(tspan4,2));

%3rd Parabolic Blend
t3=0:Ts:tb3;
tspan5=tspan4(end)+t3(2:end);
s3=s23(:,end)+v23(:,end).*t3+0.5*a3.*t3.^2;
v3=v23(:,end)+a3.*t3;
a3=a3*ones(1,size(tspan5,2));

%3rd Linear Part
t34=0:Ts:tl34;
tspan6=tspan5(end)+t34(2:end);
s34=s3(:,end)+v34(:,end).*t34;
v34=v3(:,end)*ones(1,size(t34,2));
a34=zeros(3,size(tspan6,2));

%4th Parabolic Blend
t4=0:Ts:tb4;
tspan7=tspan6(end)+t4(2:end); 
s4=s34(:,end)+v34(:,end).*t4+0.5*a4.*t4.^2;
v4=v34(:,end)+a4.*t4;
a4=a4*ones(1,size(tspan7,2));

%Total timespan and trajectories:
t_total=[tspan1,tspan2,tspan3,tspan4,tspan5,tspan6,tspan7];
s_total=[s1,s12(:,2:end),s2(:,2:end),s23(:,2:end),s3(:,2:end),s34(:,2:end),s4(:,2:end)];
v_total=[v1,v12(:,2:end),v2(:,2:end),v23(:,2:end),v3(:,2:end),v34(:,2:end),v4(:,2:end)];
a_total=[a1,a12,a2,a23,a3,a34,a4];

%Time for middle point
tmed=td12+td23/2;

%Calculate norm of Velocity and Acceleration Vectors at each timepoint
norm_V=zeros(1,size(t_total,2));
norm_A=zeros(1,size(t_total,2));

for i=1:length(t_total)
   norm_V(i)=norm(v_total(:,i));
   norm_A(i)=norm(a_total(:,i));
end

%% Animation of Trajectory
animateCylinder(2)
clear dq dv i max_pos a1 a12 a2 a23 a3 a34 a4 s1 s12 s2 s23 s3 s34 s4 v1 v12 v2 v23 v3...
      v34 v4 tspan1 tspan2 tspan3 tspan4 tspan5 tspan6 tspan7 t1 t12 t2 t23 t3 t34 t4;
