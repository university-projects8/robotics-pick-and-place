%% Inputs for Trajectory Generation

qA=[-0.0242,0.6822,0]';
qB=[0.3884,0.5967, 0.08]';
qM=[0.275,0.615,0.09]';
tf=3;
or_A=[0,0,0,1];
or_B=[0,0,0,1];
%% Selecting the value of tmed=2.1 sec and simulating Trajectory

ts=1e-6;    tmed=2.1;      % timestep and tmed
limit_V=0.35;     limit_A=0.7;   %norm limit for all three axes.


%Simulate the position, velocity and acceleration trajectories
[t_total,s_total,v_total,a_total]=trajPolynomial(qA,qB,qM,tf,tmed,ts);

%Calculate the norm at each timepoint of velocity and acceleration trajectories
len=size(t_total,2);
norm_V=zeros(1,len);
norm_A=zeros(1,len);
for i=1:len
   norm_V(i)=norm(v_total(:,i));
   norm_A(i)=norm(a_total(:,i));
end

%% Plots

figure()    % Norm of Velocity Vector at each timepoint

xpoints=[t_total(1),0;t_total(end),0];
   plot_2d(1,t_total,norm_V,limit_V,'Time','Velocity',...
            '|Velocity|','Velocity Norm during Motion',0.2,xpoints,[],[])
                  
figure()    % Norm of Acceleration Vector at each timepoint

   plot_2d(1,t_total,norm_V,limit_A,'Time','Acceleration',...
            '|Acceleration|','Acceleration Norm during Motion',0.2,[],[],[])
         
figure()    % Position Trajectory

xpoints=[t_total(end),qB(1);t_total(end),qB(2);t_total(end),qB(3);t_total(1),qA(1);...
         t_total(1),qA(2);t_total(1),qA(3);  tmed,qM(1);tmed,qM(2);tmed,qM(3)];
   plot_2d(1,t_total,s_total,[],'Time','Position', {'X-Axis';'Y-Axis';'Z-Axis'},...
            'Position per Axis during Motion',0.2,xpoints,[],[])
                  
figure()    % Position Trajectory 

subplot(3,1,1)
xpoints=[t_total(end),qB(1);t_total(1),qA(1);tmed,qM(1)];
   plot_2d(1,t_total,s_total(1,:),[],'Time','Position','X-Axis','Position X-Axis',0.2,...
            xpoints,[],[])
         
subplot(3,1,2)
xpoints=[t_total(end),qB(2);t_total(1),qA(2);tmed,qM(2)];
   plot_2d(1,t_total,s_total(2,:),[],'Time','Position',...
            'Y-Axis','Position Y-Axis',0.2,...
             xpoints,[],[])
          
subplot(3,1,3)
xpoints=[t_total(end),qB(3);t_total(1),qA(3);tmed,qM(3)];
   plot_2d(1,t_total,s_total(3,:),[],'Time','Position','Z-Axis','Position Z-Axis',0.2,...
            xpoints,[],[])
                  
figure()    % Velocity Trajectory

xpoints=[t_total(1),0;t_total(end),0];
   plot_2d(1,t_total,v_total,[limit_V/sqrt(3);-limit_V/sqrt(3)],'Time','Velocity',...
            {'X-Axis';'Y-Axis';'Z-Axis';'Limit'},'Velocity per Axis during Motion',0.2,...
             xpoints,[],[])

figure()    % Velocity Trajectory

subplot(3,1,1)
   plot_2d(1,t_total,v_total(1,:),[],'Time','Velocity','X-Axis','Velocity X-Axis',0.2,...
            xpoints,[],[])
         
subplot(3,1,2)
   plot_2d(1,t_total,v_total(2,:),[],'Time','Velocity','Y-Axis','Velocity Y-Axis',0.2,...
            xpoints,[],[])
         
subplot(3,1,3)
   plot_2d(1,t_total,v_total(3,:),[],'Time','Velocity','Z-Axis','Velocity Z-Axis',0.2,...
            xpoints,[],[])
         
figure()    % Acceleration Trajectory

   plot_2d(1,t_total,a_total,[limit_A/sqrt(3);-limit_A/sqrt(3)],'Time','Acceleration',...
            {'X-Axis';'Y-Axis';'Z-Axis';'Limit'},'Acceleration per Axis during Motion',...
               0.2,[],[],[])
            
figure()    % Acceleration Trajectory

subplot(3,1,1)
   plot_2d(1,t_total,a_total(1,:),[],'Time','Acceleration','X-Axis',...
            'Acceleration X-Axis',0.2,[],[],[])
         
subplot(3,1,2)
   plot_2d(1,t_total,a_total(2,:),[],'Time','Acceleration','Y-Axis',...
            'Acceleration Y-Axis',0.2,[],[],[])
                  
subplot(3,1,3)
   plot_2d(1,t_total,a_total(3,:),[],'Time','Acceleration','Z-Axis',...
            'Acceleration Z-Axis',0.2,[],[],[])   

%%
clear i len xpoints;
