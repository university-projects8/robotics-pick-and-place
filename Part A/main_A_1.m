%% Main File
% First thing is to select the time to pass from the middle point: tmed
% For that we need to calculate the trajectory parameters and evaluate the
% acceleration and velocity trajectories they generate in order to satisfy
% the constraints.
% Secondly, after we have selected tmed, we simulate the respective trajectory.

clear all;
close all;

%% Inputs for Trajectory Generation

qA=[-0.0242,0.6822,0]';
qB=[0.3884,0.5967, 0.08]';
qM=[0.275,0.615,0.09]';
tf=3;
or_A=[0,0,0,1];
or_B=[0,0,0,1];
%% Calculate A, V for each tmed selection (via Parameters Calculation)

[tspan,a1,a2,a3,V_max]=parameters(qA,qB,qM,tf);

%% Evaluate each A,V in order to accept/reject tmed values

limitA=0.7/sqrt(3);
limitV=0.35/sqrt(3);

[yesX,noX]=eval_time(a1(1,:),a2(1,:),a3(1,:),tspan,limitA);
[yesY,noY]=eval_time(a1(2,:),a2(2,:),a3(2,:),tspan,limitA);
[yesZ,noZ]=eval_time(a1(3,:),a2(3,:),a3(3,:),tspan,limitA);
[yesA,~]=eval_time(a1,a2,a3,tspan,limitA);
[yesV,noV]=eval_time(V_max(1,:),V_max(2,:),V_max(3,:),tspan,limitV);  

%% Acceleration and Velocity Plots

figure()    % Acceleration of X-Axis as a function of tmed at t=0,t=tmed and t=tfinal;
   xpoints=[noX',zeros(size(noX,2),1)];
   opoints=[yesX',zeros(size(yesX,2),1)];
   oopoints=[yesA',zeros(size(yesA,2),1)];
   plot_2d(1,tspan,[a1(1,:);a2(1,:);a3(1,:)],[limitA;-limitA],'Time','Acceleration',...
            {'Initial Point';'Middle Point';'Final Point';'Limit'}...
             ,'X-Axis Accelerations',0.2,xpoints,opoints,oopoints);

figure()    % Acceleration of Y-Axis as a function of tmed at t=0,t=tmed and t=tfinal;
   xpoints=[noY',zeros(size(noY,2),1)];
   opoints=[yesY',zeros(size(yesY,2),1)];
   plot_2d(1,tspan,[a1(2,:);a2(2,:);a3(2,:)],[limitA;-limitA],'Time','Acceleration',...
            {'Initial Point';'Middle Point';'Final Point';'Limit'}...
             ,'Y-Axis Accelerations',0.2,xpoints,opoints,oopoints); 
    
figure()    % Acceleration of Z-Axis as a function of tmed at t=0,t=tmed and t=tfinal;
   xpoints=[noZ',zeros(size(noZ,2),1)];
   opoints=[yesZ',zeros(size(yesZ,2),1)];
   plot_2d(1,tspan,[a1(3,:);a2(3,:);a3(3,:)],[limitA;-limitA],'Time','Acceleration',...
            {'Initial Point';'Middle Point';'Final Point';'Limit'}...
             ,'Z-Axis Accelerations',0.2,xpoints,opoints,oopoints);                                           
    
figure()    % Velocity Max as a function of tmed per Axis (X,Y,Z);
   xpoints=[noV',zeros(size(noV,2),1)];
   opoints=[yesV',zeros(size(yesV,2),1)];
   oopoints=[yesV',zeros(size(yesV,2),1)];
   plot_2d(1,tspan,V_max,limitV,'Time','Velocity Norm',{'X-Axis';'Y-Axis';'Z-Axis'...
            ;'Limit'},'Max Velocity Norm',0.2,xpoints,opoints,oopoints);   
   
%% Selecting the value of tmed=2.1 sec and simulating Trajectory

ts=1e-6;    tmed=2.1;      % timestep and tmed
limit_V=0.35;     limit_A=0.7;   %norm limit for all three axes.


%Simulate the position, velocity and acceleration trajectories
[t_total,s_total,v_total,a_total]=trajPolynomial(qA,qB,qM,tf,tmed,ts);

%Calculate the norm at each timepoint of velocity and acceleration trajectories
len=size(t_total,2);
norm_V=zeros(1,len);
norm_A=zeros(1,len);
for i=1:len
   norm_V(i)=norm(v_total(:,i));
   norm_A(i)=norm(a_total(:,i));
end

%% Plots

figure()    % Norm of Velocity Vector at each timepoint

xpoints=[t_total(1),0;t_total(end),0];
   plot_2d(1,t_total,norm_V,limit_V,'Time','Velocity',...
            '|Velocity|','Velocity Norm during Motion',0.2,xpoints,[],[])
                  
figure()    % Norm of Acceleration Vector at each timepoint

   plot_2d(1,t_total,norm_V,limit_A,'Time','Acceleration',...
            '|Acceleration|','Acceleration Norm during Motion',0.2,[],[],[])
         
figure()    % Position Trajectory

xpoints=[t_total(end),qB(1);t_total(end),qB(2);t_total(end),qB(3);t_total(1),qA(1);...
         t_total(1),qA(2);t_total(1),qA(3);  tmed,qM(1);tmed,qM(2);tmed,qM(3)];
   plot_2d(1,t_total,s_total,[],'Time','Position', {'X-Axis';'Y-Axis';'Z-Axis'},...
            'Position per Axis during Motion',0.2,xpoints,[],[])
                  
figure()    % Position Trajectory 

subplot(3,1,1)
xpoints=[t_total(end),qB(1);t_total(1),qA(1);tmed,qM(1)];
   plot_2d(1,t_total,s_total(1,:),[],'Time','Position','X-Axis','Position X-Axis',0.2,...
            xpoints,[],[])
         
subplot(3,1,2)
xpoints=[t_total(end),qB(2);t_total(1),qA(2);tmed,qM(2)];
   plot_2d(1,t_total,s_total(2,:),[],'Time','Position',...
            'Y-Axis','Position Y-Axis',0.2,...
             xpoints,[],[])
          
subplot(3,1,3)
xpoints=[t_total(end),qB(3);t_total(1),qA(3);tmed,qM(3)];
   plot_2d(1,t_total,s_total(3,:),[],'Time','Position','Z-Axis','Position Z-Axis',0.2,...
            xpoints,[],[])
                  
figure()    % Velocity Trajectory

xpoints=[t_total(1),0;t_total(end),0];
   plot_2d(1,t_total,v_total,[limit_V/sqrt(3);-limit_V/sqrt(3)],'Time','Velocity',...
            {'X-Axis';'Y-Axis';'Z-Axis';'Limit'},'Velocity per Axis during Motion',0.2,...
             xpoints,[],[])

figure()    % Velocity Trajectory

subplot(3,1,1)
   plot_2d(1,t_total,v_total(1,:),[],'Time','Velocity','X-Axis','Velocity X-Axis',0.2,...
            xpoints,[],[])
         
subplot(3,1,2)
   plot_2d(1,t_total,v_total(2,:),[],'Time','Velocity','Y-Axis','Velocity Y-Axis',0.2,...
            xpoints,[],[])
         
subplot(3,1,3)
   plot_2d(1,t_total,v_total(3,:),[],'Time','Velocity','Z-Axis','Velocity Z-Axis',0.2,...
            xpoints,[],[])
         
figure()    % Acceleration Trajectory

   plot_2d(1,t_total,a_total,[limit_A/sqrt(3);-limit_A/sqrt(3)],'Time','Acceleration',...
            {'X-Axis';'Y-Axis';'Z-Axis';'Limit'},'Acceleration per Axis during Motion',...
               0.2,[],[],[])
            
figure()    % Acceleration Trajectory

subplot(3,1,1)
   plot_2d(1,t_total,a_total(1,:),[],'Time','Acceleration','X-Axis',...
            'Acceleration X-Axis',0.2,[],[],[])
         
subplot(3,1,2)
   plot_2d(1,t_total,a_total(2,:),[],'Time','Acceleration','Y-Axis',...
            'Acceleration Y-Axis',0.2,[],[],[])
                  
subplot(3,1,3)
   plot_2d(1,t_total,a_total(3,:),[],'Time','Acceleration','Z-Axis',...
            'Acceleration Z-Axis',0.2,[],[],[])   

%% Animation of Trajectory      
animateCylinder(1)

%% We need to save the trajectories for next questions
save('trajectory3rdOrder.mat','t_total','s_total','v_total')

%%
clear i len noV noX noY noZ oopoints opoints yesA yesV yesX yesY yesZ xpoints;
