%% Inputs for Trajectory Generation

qA=[-0.0242,0.6822,0]';
qB=[0.3884,0.5967, 0.08]';
qM=[0.275,0.615,0.09]';
tf=3;
or_A=[0,0,0,1];
or_B=[0,0,0,1];
%% Selecting the value of tmed=2.1 sec and simulating Trajectory

ts=1e-6;    tmed=2.1;      % timestep and tmed
limit_V=0.35;     limit_A=0.7;   %norm limit for all three axes.


%Simulate the position, velocity and acceleration trajectories
[t_total,s_total,v_total,a_total]=trajPolynomial(qA,qB,qM,tf,tmed,ts);

%Calculate the norm at each timepoint of velocity and acceleration trajectories
len=size(t_total,2);
norm_V=zeros(1,len);
norm_A=zeros(1,len);
for i=1:len
   norm_V(i)=norm(v_total(:,i));
   norm_A(i)=norm(a_total(:,i));
end
%% Animation of Trajectory      
animateCylinder(1)

%%
clear i len;
