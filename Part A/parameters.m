function [tau,a1,a2,a3,V_max]= parameters(qA,qB,qM,tf)
% Create total timespan for simulation
tau=0.5:0.05:2.5;
len=size(tau,2);

a1=zeros(3,len);
a2=zeros(3,len);
a3=zeros(3,len);
V_max=zeros(3,len);

for i=1:len
   
   tmed=tau(i);      % this will be the time to pass through the qM
   
   k12= -(qA - qM + (tmed^3*(qA*tf^2 - 3*qA*tmed^2 + 3*qB*tmed^2 - qM*tf^2 + ...
      2*qA*tf*tmed - 2*qM*tf*tmed))/(2*tf^2*tmed^3 - 2*tf*tmed^4))/tmed^2;
   
   a1(:,i)=2*k12;    % Acceleration at t=0
   
   k22=(3*tmed*(qA*tf^2 - 3*qA*tmed^2 + 3*qB*tmed^2 - qM*tf^2 + 2*qA*tf*tmed - 2*qM*tf*tmed))...
   /(2*tf^2*tmed^3 - 2*tf*tmed^4) - (qA - qM + (tmed^3*(qA*tf^2 - 3*qA*tmed^2 + 3*qB*tmed^2 ...
   - qM*tf^2 + 2*qA*tf*tmed - 2*qM*tf*tmed))/(2*tf^2*tmed^3 - 2*tf*tmed^4))/tmed^2;
   
   a2(:,i)=2*k22;    % Acceleration at qM
   
   k23=(qB - qM + (tf - tmed)^2*((qA - qM + (tmed^3*(qA*tf^2 - 3*qA*tmed^2 + 3*qB*tmed^2 ...
   - qM*tf^2 + 2*qA*tf*tmed - 2*qM*tf*tmed))/(2*tf^2*tmed^3 - 2*tf*tmed^4))/tmed^2 - ...
   (3*tmed*(qA*tf^2 - 3*qA*tmed^2 + 3*qB*tmed^2 - qM*tf^2 + 2*qA*tf*tmed - 2*qM*tf*tmed))...
   /(2*tf^2*tmed^3 - 2*tf*tmed^4)) + ((2*(qA - qM + (tmed^3*(qA*tf^2 - 3*qA*tmed^2 + ...
   3*qB*tmed^2 - qM*tf^2 + 2*qA*tf*tmed - 2*qM*tf*tmed))/(2*tf^2*tmed^3 - 2*tf*tmed^4)))...
   /tmed - (3*tmed^2*(qA*tf^2 - 3*qA*tmed^2 + 3*qB*tmed^2 - qM*tf^2 + 2*qA*tf*tmed...
   - 2*qM*tf*tmed))/(2*tf^2*tmed^3 - 2*tf*tmed^4))*(tf - tmed))/(tf - tmed)^3;
   
   a3(:,i)=2*k22+6*k23*(3-tmed); %acceleration at tf=3
  
   k13 = (qA*tf^2 - 3*qA*tmed^2 + 3*qB*tmed^2 - qM*tf^2 + 2*qA*tf*tmed - 2*qM*tf*tmed)...
     /(2*tf^2*tmed^3 - 2*tf*tmed^4);
  
   % If the 1st part has a point where acceleration=0, then this happens at that time point
   t_max1=-k12./(3*k13);   
   % If the 2nd part has a point where acceleration=0, then this happens at that time point
   t_max2=-k22./(3*k23);
   
   
   k21=(3*tmed^2*(qA*tf^2 - 3*qA*tmed^2 + 3*qB*tmed^2 - qM*tf^2 + 2*qA*tf*tmed - 2*qM*tf*tmed))...
   /(2*tf^2*tmed^3 - 2*tf*tmed^4) - (2*(qA - qM + (tmed^3*(qA*tf^2 - 3*qA*tmed^2 + 3*qB*tmed^2 -...
   qM*tf^2 + 2*qA*tf*tmed - 2*qM*tf*tmed))/(2*tf^2*tmed^3 - 2*tf*tmed^4)))/tmed; 
 
   % Initialize these max velocities as zeros
   v1=zeros(3,1); v2=zeros(3,1); v_max=zeros(3,1);

   for k=1:3
      
      % If time max is valid per axis for the 1st part
      if (t_max1(k)>0 && t_max1(k)<tmed)
         
         % This is the velocity(k) at time=tmax1(k)
         v1(k)=-k12(k)^2/(3*k13(k));
         
      end
      
      % If time max is valid per axis for the 2nd part
      if (t_max2(k)>0 && t_max2(k)<(3-tmed))
         
         % This is the velocity(k) at time=tmax2(k)
         v2(k)=k21(k)-k22(k)^2/(3*k23(k));
         
      end
      
      % We store the max one per axis (we compare the 2 trajectory parts)
      v_max(k)=max([v1(k),v2(k)]);
      
   end

   V_max(:,i)=v_max;  
end
end
