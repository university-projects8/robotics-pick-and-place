clear all
format long g

syms tmed tf  
assume(tmed>0)
assume(tf>tmed)

syms qA qB qM 
syms k10 k11 k12 k13
syms k20 k21 k22 k23 

eq1= k10==qA;

eq2= k11==0;

eq3= k10+k11*tmed+k12*tmed^2+k13*tmed^3==qM;

eq4= k20==qM;

eq5= k20+k21*(tf-tmed)+k22*(tf-tmed)^2+k23*(tf-tmed)^3==qB;

eq6= k21+2*k22*(tf-tmed)+3*k23*(tf-tmed)^2==0;

eq7= k21==2*k12*tmed+3*k13*tmed^2;

eq8= k22==k12+3*k13*tmed;

% Replace Eqns 1,2,4 in the Eqns 3,5 where they are involved:
eq3=subs(eq3,[lhs(eq1),lhs(eq2),lhs(eq4)],[rhs(eq1),rhs(eq2),rhs(eq4)]);
eq5=subs(eq5,[lhs(eq1),lhs(eq2),lhs(eq4)],[rhs(eq1),rhs(eq2),rhs(eq4)]);

% Replace Eqns 7 and 8 into Eqns 5,6:
eq5=subs(eq5,[lhs(eq7),lhs(eq8)],[rhs(eq7),rhs(eq8)]);
eq6=subs(eq6,[lhs(eq7),lhs(eq8)],[rhs(eq7),rhs(eq8)]);

%Now, Eqns 3,5,6 formulate qA 3x3 System of Eqns with 3 unknown variables.
%Step by step, we replace parameters to find the solution:

eq5=isolate(eq5,k23);        %Solve Eqn 5 based on k_23
eq6=subs(eq6,lhs(eq5),rhs(eq5));   %Replace k_23 into Eqn 6 based on Eqn 5
eqn3=subs(eq3,lhs(eq5),rhs(eq5));    %Replace k_23 into Eqn 3 based on Eqn 5
eqn3=isolate(eqn3,k12);      %Solve Eqn 3 based on k_12
eq6=subs(eq6,lhs(eqn3),rhs(eqn3));   %Replace k_12 into Eqn 6 based on Eqn 3

%Now, all we have is to isolate k_13 and then move backwards

k13_eqn=isolate(eq6,k13);

k12_eqn=subs(eqn3,lhs(k13_eqn),rhs(k13_eqn));

k23_eqn=subs(eq5,[lhs(k13_eqn),lhs(k12_eqn)],[rhs(k13_eqn),rhs(k12_eqn)]);

k22_eqn=subs(eq8,[lhs(k13_eqn),lhs(k12_eqn)],[rhs(k13_eqn),rhs(k12_eqn)]);

k21_eqn=subs(eq7,[lhs(k13_eqn),lhs(k12_eqn)],[rhs(k13_eqn),rhs(k12_eqn)]);
