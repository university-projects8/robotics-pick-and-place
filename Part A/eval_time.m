%% Evaluate which t_med selections are appropriate
% Inputs: Vectors x1, x2, x3 are the vectors with the acceleration/velocity value.
%         Vector t is the different t_med possibilities.
%         Scalar lim: is the limit which x vectors must not exceed (as absolute value).
% A value of the t vector, e.g. at location k, is accepted if all x vectors at that
% location are between [-lim,lim].

function [accept,reject]=eval_time(x1,x2,x3,t,lim)
   accept=[];reject=[];
   len=size(t,2);
   for i=1:len
      x=[x1(:,i);x2(:,i);x3(:,i)];
      if all(abs(x)<=lim)
         accept=[accept,t(i)];
      else
         reject=[reject,t(i)];
      end
   end
end
