%% Inputs for Trajectory Generation

qA=[-0.0242,0.6822,0]';
qB=[0.3884,0.5967, 0.08]';
qM=[0.275,0.615,0.09]';
tf=3;
or_A=[0,0,0,1];
or_B=[0,0,0,1];
%% Calculate A, V for each tmed selection (via Parameters Calculation)

[tspan,a1,a2,a3,V_max]=parameters(qA,qB,qM,tf);

%% Evaluate each A,V in order to accept/reject tmed values

limitA=0.7/sqrt(3);
limitV=0.35/sqrt(3);

[yesX,noX]=eval_time(a1(1,:),a2(1,:),a3(1,:),tspan,limitA);
[yesY,noY]=eval_time(a1(2,:),a2(2,:),a3(2,:),tspan,limitA);
[yesZ,noZ]=eval_time(a1(3,:),a2(3,:),a3(3,:),tspan,limitA);
[yesA,~]=eval_time(a1,a2,a3,tspan,limitA);
[yesV,noV]=eval_time(V_max(1,:),V_max(2,:),V_max(3,:),tspan,limitV);  

%% Acceleration and Velocity Plots

figure()    % Acceleration of X-Axis as a function of tmed at t=0,t=tmed and t=tfinal;
   xpoints=[noX',zeros(size(noX,2),1)];
   opoints=[yesX',zeros(size(yesX,2),1)];
   oopoints=[yesA',zeros(size(yesA,2),1)];
   plot_2d(1,tspan,[a1(1,:);a2(1,:);a3(1,:)],[limitA;-limitA],'Time','Acceleration',...
            {'Initial Point';'Middle Point';'Final Point';'Limit'}...
             ,'X-Axis Accelerations',0.2,xpoints,opoints,oopoints);

figure()    % Acceleration of Y-Axis as a function of tmed at t=0,t=tmed and t=tfinal;
   xpoints=[noY',zeros(size(noY,2),1)];
   opoints=[yesY',zeros(size(yesY,2),1)];
   plot_2d(1,tspan,[a1(2,:);a2(2,:);a3(2,:)],[limitA;-limitA],'Time','Acceleration',...
            {'Initial Point';'Middle Point';'Final Point';'Limit'}...
             ,'Y-Axis Accelerations',0.2,xpoints,opoints,oopoints); 
    
figure()    % Acceleration of Z-Axis as a function of tmed at t=0,t=tmed and t=tfinal;
   xpoints=[noZ',zeros(size(noZ,2),1)];
   opoints=[yesZ',zeros(size(yesZ,2),1)];
   plot_2d(1,tspan,[a1(3,:);a2(3,:);a3(3,:)],[limitA;-limitA],'Time','Acceleration',...
            {'Initial Point';'Middle Point';'Final Point';'Limit'}...
             ,'Z-Axis Accelerations',0.2,xpoints,opoints,oopoints);                                           
    
figure()    % Velocity Max as a function of tmed per Axis (X,Y,Z);
   xpoints=[noV',zeros(size(noV,2),1)];
   opoints=[yesV',zeros(size(yesV,2),1)];
   oopoints=[yesV',zeros(size(yesV,2),1)];
   plot_2d(1,tspan,V_max,limitV,'Time','Velocity Norm',{'X-Axis';'Y-Axis';'Z-Axis'...
            ;'Limit'},'Max Velocity Norm',0.2,xpoints,opoints,oopoints);  

%%
clear opoints oopoints xpoints noV noX noY noZ yesA yesV yesX yesY yesZ
