%% Generate 3rd Order Polynomial Trajectory
% Input: Initial, Middle and Final Point, as well as total movement time, time to pass
%        through the middle point and timestep.
% Output: Trajectories of position,velocity and acceleration. Also, the total timespan.

function [tspan,s_tot,v_tot,a_tot] = trajPolynomial(qA,qB,qM,tf,tmed,ts)
%% Parameters Calculation
%Firstly, we will calculate the parameters of the trajectory, based on simplified eqns
%from the parameters_gen file.

k10=qA;
k11=zeros(3,1);
k20=qM;

k13=-(3*qA*tmed^2-qA*tf^2-3*qB*tmed^2+qM*tf^2-2*qA*tmed*tf+2*qM*tmed*tf)...
      /(2*tmed^3*tf*(tf-tmed));
k12=-(k13*tmed^3+qA-qM)/tmed^2;  %Based on k13
k23=(qB-qM-(3*k13*tmed^2+2*k12*tmed)*(tf-tmed)-(k12+3*k13*tmed)*(tf-tmed)^2)...
      /(tf-tmed)^3;     %Based on k12, k13
k22=k12+3*k13*tmed;     %Based on k12, k13
k21=2*k12*tmed+3*k13*tmed^2;     %Based on k12, k13

%% Simulation

%Timespans for the two different parts of the trajectory
tspan1=0:ts:tmed;
tspan2=tmed:ts:tf;

% Position Trajectory for each part 
s1=k10+k11*tspan1+k12*tspan1.^2+k13*tspan1.^3;
s2=k20+k21*(tspan2-tmed)+k22*(tspan2-tmed).^2+k23*(tspan2-tmed).^3;

% Velocity Trajectory for each part 
v1=k11+2*k12*tspan1+3*k13*tspan1.^2;
v2=k21+2*k22*(tspan2-tmed)+3*k23*(tspan2-tmed).^2;

% Acceleration Trajectory for each part 
a1=2*k12+6*k13*tspan1;
a2=2*k22+6*k23*(tspan2-tmed);

% Integrate in a total trajectory removing duplicate points :)
s_tot=[s1, s2(:,2:end)];
v_tot=[v1, v2(:,2:end)];
a_tot=[a1, a2(:,2:end)];
tspan=[tspan1, tspan2(2:end)];



end

