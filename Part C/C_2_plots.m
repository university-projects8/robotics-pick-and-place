clear all; close all;
%% Read Data files
fileID1 = fopen('trajectory_pick_EA.dat.txt');
fileID2 = fopen('trajectory_place_AB.dat.txt');
formatSpec='%f';
pick = fscanf(fileID1,formatSpec);
place = fscanf(fileID2,formatSpec);
data1=[pick(1:1801,1)';pick(1802:3602,1)';pick(3603:5403,1)'];
data2=[place(1:1900,1)';place(1901:3800,1)';place(3801:5700,1)'];
fclose(fileID1);
fclose(fileID2);
clear fileID1 fileID2 formatSpec

%Using that frequency of data is 500 Hz, hence dt=2ms, we can find total time of each
%movement and create the respective timespans

dt=2e-3;
t_tot_1=size(data1,2)*dt;
t_tot_2=size(data2,2)*dt;

%% Parameters for Learning
tau=1;
az=50;
bz=az/4;
N=75;

%% First Dynamic Movement Primitive
% y1: the position
y1=data1;
len1=size(y1,2);

% Starting Position and Final one
y01=y1(:,1); 
g1=y1(:,end);

% Acceleration and Velocity Trajectories based on Position Data
% using v=dx/dt, a=dv/dt
yd1=zeros(3,size(data1,2));
ydd1=zeros(3,size(data1,2));
yd1(1)=0;
ydd1(1)=0;

for i=2:len1
   yd1(:,i)=(y1(:,i)-y1(:,i-1))/dt;
   ydd1(:,i)=(yd1(:,i)-yd1(:,i-1))/dt;
end

%Desirable Forcing Term and training
Fd=eye(3)/(diag(g1-y01))*(tau^2*ydd1-az*(bz*(g1-y1))-tau*yd1);  
[c1,h1,w1,ax1]=learn_weights(Fd,y01,g1,tau,t_tot_1,N,dt);

%Forcing Term is a function of x, the canonical variable.
% About x: tau*xdot=-ax*x, x0=1;
x=zeros(1,len1);
xdot=zeros(1,len1);
x(1)=1; xdot(1)=0;
for k=2:len1
   xdot(k)=-ax1*x(k-1)/tau;
   x(k)=x(k-1)+xdot(k)*dt;
end
% Calculate F after training via Gaussians
F1=zeros(3,size(data1,2));
for k=1:len1
F_num=0;
F_den=0;
   for i=1:N
      Psi=exp(-h1(i)*(x(k)-c1(i))^2);
      F_num=F_num+w1(i,:)'*Psi;
      F_den=F_den+Psi;
   end
  F1(:,k)=F_num*x(k)/F_den;
end

%Now it's time for the differential equations of our system
zd_dmp1=zeros(3,len1); 
yd_dmp1=zeros(3,len1);
y_dmp1=zeros(3,len1);
z_dmp1=zeros(3,len1);

%Initial values:
y_dmp1(:,1)=y1(:,1);
yd_dmp1(:,1)=zeros(3,1);
z_dmp1(:,1)=yd_dmp1(:,1)/tau;
zd_dmp1(:,1)=az*(bz*(g1-y_dmp1(:,1))-z_dmp1(:,1))+diag(g1-y01)*F1(:,1);

for k=2:len1
   y_dmp1(:,k)=y_dmp1(:,k-1)+yd_dmp1(:,k-1)*dt;
   z_dmp1(:,k)=z_dmp1(:,k-1)+zd_dmp1(:,k-1)*dt;
   yd_dmp1(:,k)=z_dmp1(:,k)/tau;
   zd_dmp1(:,k)=az*(bz*(g1-y_dmp1(:,k))-z_dmp1(:,k))+diag(g1-y01)*F1(:,k);  
end

%% Second Dynamic Movement Primitive
% y2: the position
y2=data2;
len2=size(y2,2);

% initial value and final one
y02=y2(:,1); 
g2=y2(:,end);

%Accel and Velocity Trajectories
yd2=zeros(3,len2);
ydd2=zeros(3,len2);
yd2(1)=0;
ydd2(1)=0;

for i=2:len2
   yd2(:,i)=(y2(:,i)-y2(:,i-1))/dt;
   ydd2(:,i)=(yd2(:,i)-yd2(:,i-1))/dt;
end
%Desirable Forcing Term and training
Fd2=eye(3)/(diag(g2-y02))*(tau^2*ydd2-az*(bz*(g2-y2))-tau*yd2);  
[c2,h2,w2,ax2]=learn_weights(Fd2,y02,g2,tau,t_tot_2,N,dt);

%Forcing Term is a function of x
% About x: txdot=-ax*x, x0=1;
x2=zeros(1,len2);
xdot2=zeros(1,len2);
x2(1)=1; xdot(1)=0;
for k=2:len2
   xdot2(k)=-ax2*x2(k-1)/tau;
   x2(k)=x2(k-1)+xdot2(k)*dt;
end

%Calculate F after training via Gaussians
F2=zeros(3,size(data2,2));
for k=1:len2
F_num=0;
F_den=0;
   for i=1:N
      Psi=exp(-h2(i)*(x2(k)-c2(i))^2);
      F_num=F_num+w2(i,:)'*Psi;
      F_den=F_den+Psi;
   end
  F2(:,k)=F_num*x2(k)/F_den;
end
% HERE is the new target:
g2=[0.5,0.45,0.18]';

% Differential Equations of the system
zd_dmp2=zeros(3,len2); 
yd_dmp2=zeros(3,len2);
y_dmp2=zeros(3,len2);
z_dmp2=zeros(3,len2);

y_dmp2(:,1)=y2(:,1);
yd_dmp2(:,1)=zeros(3,1);
z_dmp2(:,1)=yd_dmp2(:,1)/tau;
zd_dmp2(:,1)=az*(bz*(g2-y_dmp2(:,1))-z_dmp2(:,1))+diag(g2-y02)*F2(:,1);
for k=2:len2
   y_dmp2(:,k)=y_dmp2(:,k-1)+yd_dmp2(:,k-1)*dt;
   z_dmp2(:,k)=z_dmp2(:,k-1)+zd_dmp2(:,k-1)*dt;
   yd_dmp2(:,k)=z_dmp2(:,k)/tau;
   zd_dmp2(:,k)=az*(bz*(g2-y_dmp2(:,k))-z_dmp2(:,k))+diag(g2-y02)*F2(:,k);  
end

%Total Trajectories of Position and Velocity
y_dmp_new=[y_dmp1,y_dmp2];
v_dmp_tot=[yd_dmp1,yd_dmp2];

%% Simulation of Movement
global RobotArm;
RobotArm=lwr_create();
len=size(v_dmp_tot,2);
global Thetas;
global HomoRobotArm;
HomoRobotArm=zeros(4,4,len);    % Homogeneous Matrix for the Tool
Thetas=zeros(len,6);          % Joints' Configurations
qdot=zeros(len,6);            % Joints' Angular Velocity

global joints_0;              %Initial Configuration
joints_0=[2.1595, -0.6695, 1.2719, 3.1416, 1.2002, -0.9821]';
joints_now=joints_0;
% Forward Kinematics to calculate the rotation matrix between inertial frame and tool's one
g=RobotArm.fkine(joints_now);
% Accuracy of 4 decimal digits
R_0E=round(1e+4*[g.n,g.o,g.a])/1e+4;

for k=1:len
   % Calculate the Jacobian of the tool (Je)
   jacobian=RobotArm.jacobe(joints_now');
   % Calculate the Joints' Angular Velocity given v_dmp_tot (transformed through R_0E)
   q_dot=(eye(6)/jacobian)*[R_0E'*v_dmp_tot(:,k);zeros(3,1)];
   % Euler Integration for joint angles
   joints_now=joints_now+q_dot*dt;
   % Forward Kinematics to find next position and orientation
   g=RobotArm.fkine(joints_now);
   R_0E=round(1e+4*[g.n,g.o,g.a])/1e+4;
   pE_now=transl(g)';
   HomoRobotArm(:,:,k)=[R_0E,pE_now;zeros(1,3),1];
   Thetas(k,:)=joints_now';
   qdot(k,:)=q_dot;
end

startCylinder=size(yd_dmp1,2);

%% Total Data to be plotted
%The position of the tool during the movement based on the stored Homogeneous Matrices
ptool=transl(HomoRobotArm)';

%The orientation of the tool during the movement based on the stored Homogeneous Matrices
quats=rotm2quat(HomoRobotArm(1:3,1:3,:))';


%% PLOT SECTION
% The initial position of the tool and the respective homogeneous transformation matrix 
pE_0=[-0.3397, 0.5088, 0.4049]';    %(3x1)
   
% The intermediate - "pick" position.
pE_1=[-0.0242, 0.6822, 0.1]';    %(3x1)

%The final position of the tool and the respective homogeneous matrix
pE_2=g2;

% This is the desired orientation during the whole movement.
q_d=[0,0,1,0];

global v_max;
v_max=0.35;
global a_max;
a_max=0.7;
tspan=0:dt:(len-1)*dt;
xtickstep=round(tspan(end))/10;

figure()
   plot_3d(ptool(1,:)',ptool(2,:)',ptool(3,:)',20,startCylinder)
   hold off

figure()
   plot_3d(ptool(1,:)',ptool(2,:)',ptool(3,:)',20,startCylinder)
   plotcube([0.6 1.2 0.001],[ -0.1 0 -0.001],1,[0.25,0.25,0.25]);
   plotcube([+0.2 +0.6 +0.08],[ +0.3 +0.3 0],1,[255/255 229/255 204/255]);
   hold off

figure()
   plot_3d(ptool(1,:)',ptool(2,:)',ptool(3,:)',20,startCylinder)
   plotcube([0.6 1.2 0.001],[ -0.1 0 -0.001],1,[0.25,0.25,0.25]);
   plotcube([+0.2 +0.6 +0.08],[ +0.3 +0.3 0],1,[1 229/255 204/255]);
   
% Create the cylinder for frame A
[X1,Y1,Z1] = cylinder(0.025,1000);
Z1=Z1*0.1;  %setting height of cylinder
X=ptool(1,:)';Y=ptool(2,:)';Z=ptool(3,:)';
hc=mesh(X1+X(startCylinder),Y1+Y(startCylinder),(Z1+Z(startCylinder)-0.1));      
set(hc,'EdgeColor',[0.61 0.51 0.74])
%Plot the cylinder twice for visual purposes
hd=mesh(X1+X(end),Y1+Y(end),(Z1+Z(end)-0.1));
set(hd,'EdgeColor',[0.61 0.51 0.74])
hold off

figure()
   plot_2d(0,tspan,ptool,[],'Time','Tool Position',...
        {'X-Axis';'Y-Axis';'Z-Axis'},'Tool Position',...
         xtickstep,[tspan(1),pE_0(1);tspan(1),pE_0(2);tspan(1),pE_0(3);...
         tspan(startCylinder),pE_1(1);tspan(startCylinder),pE_1(2);tspan(startCylinder),pE_1(3);...
         tspan(end),pE_2(1);tspan(end),pE_2(2);tspan(end),pE_2(3)],[],[])
figure()
   subplot(3,1,1)
   plot_2d(0,tspan,ptool(1,:),[],'Time','Position','X-Axis','Tool-X Position',...
            xtickstep,[tspan(1),pE_0(1);tspan(startCylinder),pE_1(1);tspan(end),pE_2(1)],[],[])
   subplot(3,1,2)
   plot_2d(0,tspan,ptool(2,:),[],'Time','Position','Y-Axis','Tool-Y Position',...
            xtickstep,[tspan(1),pE_0(2); tspan(startCylinder),pE_1(2);tspan(end),pE_2(2)],[],[])
   subplot(3,1,3)
   plot_2d(0,tspan,ptool(3,:),[],'Time','Position','Z-Axis','Tool-Z Position',...
            xtickstep,[tspan(1),pE_0(3); tspan(startCylinder),pE_1(3);tspan(end),pE_2(3)],[],[])


figure()
   plot_2d(0,tspan,quats,[],'Time','Tool Orientation',...
        {'Scalar';'Vector1';'Vector2';'Vector3'},'Tool Orientation',...
         xtickstep,[tspan(1),q_d(1);tspan(1),q_d(2);tspan(1),q_d(3);tspan(1),q_d(4);...
         tspan(end),q_d(1);tspan(end),q_d(2);tspan(end),q_d(3);tspan(end),q_d(4)],[],[])
figure()
   subplot(4,1,1)
   plot_2d(1,tspan,quats(1,:),[],'Time','Orientation','Scalar','Tool q0',...
            xtickstep,[tspan(1),q_d(1);tspan(end),q_d(1)],[],[])
   subplot(4,1,2)
   plot_2d(1,tspan,quats(2,:),[],'Time','Orientation','Vector1','Tool q1',...
            xtickstep,[tspan(1),q_d(2);tspan(end),q_d(2)],[],[])
   subplot(4,1,3)
   plot_2d(1,tspan,quats(3,:),[],'Time','Orientation','Vector2','Tool q2',...
            xtickstep,[tspan(1),q_d(3);tspan(end),q_d(3)],[],[])
   subplot(4,1,4)
   plot_2d(1,tspan,quats(4,:),[],'Time','Orientation','Vector3','Tool q3',...
            xtickstep,[tspan(1),q_d(4);tspan(end),q_d(4)],[],[])

% Joints Position in rad
figure()
plot_2d(0,tspan,Thetas',[],'Time','Angle',...
        {'Joint1';'Joint2';'Joint3';'Joint4';'Joint5';'Joint6'},'Joints Angles (rad)',...
         xtickstep,[],[],[])
figure()
   subplot(3,2,1)
   plot_2d(0,tspan,Thetas(:,1)',[],'Time','Angle','Joint1','Joint 1 Angle (rad)',xtickstep,[],[],[])
   subplot(3,2,3)
   plot_2d(0,tspan,Thetas(:,2)',[],'Time','Angle','Joint2','Joint 2 Angle (rad)',xtickstep,[],[],[])
   subplot(3,2,5)
   plot_2d(0,tspan,Thetas(:,3)',[],'Time','Angle','Joint3','Joint 3 Angle (rad)',xtickstep,[],[],[])
   subplot(3,2,2)
   plot_2d(0,tspan,Thetas(:,4)',[],'Time','Angle','Joint4','Joint 4 Angle (rad)',xtickstep,[],[],[])
   subplot(3,2,4)
   plot_2d(0,tspan,Thetas(:,5)',[],'Time','Angle','Joint5','Joint 5 Angle (rad)',xtickstep,[],[],[])
   subplot(3,2,6)
   plot_2d(0,tspan,Thetas(:,6)',[],'Time','Angle','Joint6','Joint 6 Angle (rad)',xtickstep,[],[],[])

% Joints Position in degrees
Thetasdeg=rad2deg(Thetas);

figure()
   plot_2d(0,tspan,Thetasdeg',[],'Time','Angle',...
        {'Joint1';'Joint2';'Joint3';'Joint4';'Joint5';'Joint6'},'Joints Angles (deg)',...
         xtickstep,[],[],[])
figure()
   subplot(3,2,1)
   plot_2d(0,tspan,Thetasdeg(:,1)',[],'Time','Angle','Joint1','Joint 1 Angle (deg)',xtickstep,[],[],[])
   subplot(3,2,3)
   plot_2d(0,tspan,Thetasdeg(:,2)',[],'Time','Angle','Joint2','Joint 2 Angle (deg)',xtickstep,[],[],[])
   subplot(3,2,5)
   plot_2d(0,tspan,Thetasdeg(:,3)',[],'Time','Angle','Joint3','Joint 3 Angle (deg)',xtickstep,[],[],[])
   subplot(3,2,2)
   plot_2d(0,tspan,Thetasdeg(:,4)',[],'Time','Angle','Joint4','Joint 4 Angle (deg)',xtickstep,[],[],[])
   subplot(3,2,4)
   plot_2d(0,tspan,Thetasdeg(:,5)',[],'Time','Angle','Joint5','Joint 5 Angle (deg)',xtickstep,[],[],[])
   subplot(3,2,6)
   plot_2d(0,tspan,Thetasdeg(:,6)',[],'Time','Angle','Joint6','Joint 6 Angle (deg)',xtickstep,[],[],[])

% Joints Velocity in rad/s
figure()
   plot_2d(0,tspan,qdot',[],'Time','Angular Velocity',...
        {'Joint1';'Joint2';'Joint3';'Joint4';'Joint5';'Joint6'},'Joints Angular Velocity (rad/s)',...
         xtickstep,[],[],[])
figure()
   subplot(3,2,1)
   plot_2d(0,tspan,qdot(:,1)',[],'Time','Ang Vel','Joint1','Joint 1 Ang Vel (rad/s)',...
      xtickstep,[tspan(1),0;tspan(end),0],[],[])
   subplot(3,2,3)
   plot_2d(0,tspan,qdot(:,2)',[],'Time','Ang Vel','Joint2','Joint 2 Ang Vel (rad/s)',...
      xtickstep,[tspan(1),0;tspan(end),0],[],[])
   subplot(3,2,5)
   plot_2d(0,tspan,qdot(:,3)',[],'Time','Ang Vel','Joint3','Joint 3 Ang Vel (rad/s)',...
      xtickstep,[tspan(1),0;tspan(end),0],[],[])
   subplot(3,2,2)
   plot_2d(0,tspan,qdot(:,4)',[],'Time','Ang Vel','Joint4','Joint 4 Ang Vel (rad/s)',...
      xtickstep,[tspan(1),0;tspan(end),0],[],[])
   subplot(3,2,4)
   plot_2d(0,tspan,qdot(:,5)',[],'Time','Ang Vel','Joint5','Joint 5 Ang Vel (rad/s)',...
      xtickstep,[tspan(1),0;tspan(end),0],[],[])
   subplot(3,2,6)
   plot_2d(0,tspan,qdot(:,6)',[],'Time','Ang Vel','Joint6','Joint 6 Ang Vel (rad/s)',...
      xtickstep,[tspan(1),0;tspan(end),0],[],[])


% Tool's Velocity in m/s
figure()
   plot_2d(0,tspan,v_dmp_tot,[],'Time','Linear Velocity',...
        {'X-Axis';'Y-Axis';'Z-Axis'},'Tool Linear Velocity (m/s)',...
         xtickstep,[tspan(1),0;tspan(end),0],[],[])
figure()
   subplot(3,1,1)
   plot_2d(0,tspan,v_dmp_tot(1,:),[],'Time','Ang Vel','X-Axis','Tool-X Velocity',...
      xtickstep,[tspan(1),0;tspan(end),0],[],[])
   subplot(3,1,2)
   plot_2d(0,tspan,v_dmp_tot(2,:),[],'Time','Ang Vel','Y-Axis','Tool-Y Velocity',...
      xtickstep,[tspan(1),0;tspan(end),0],[],[])
   subplot(3,1,3)
   plot_2d(0,tspan,v_dmp_tot(3,:),[],'Time','Ang Vel','Z-Axis','Tool-Z Velocity',...
      xtickstep,[tspan(1),0;tspan(end),0],[],[])

% Tool's Velocity in m/s
normvtool=zeros(1,len);
for k=1:len
   normvtool(k)=norm(v_dmp_tot(:,k));
end

figure()
   plot_2d(0,tspan,normvtool,v_max,'Time','Tool Linear Velocity Norm',...
        {'|Velocity|'},'Tool Linear Velocity (m/s)',...
         xtickstep,[tspan(1),0;tspan(end),0],[],[])
%Comparison Figure()
MoveIt=load('DMP_normal.mat');
y_dmp_old=MoveIt.y_dmp_tot;
figure() 
   plot_compare(0,tspan,y_dmp_old,y_dmp_new,[],'Time','Tool Position',...
                     {'X-prev';'Y-prev';'Z-prev';'X-new';'Y-new';'Z-new'},...
                     'Tool Position',xtickstep,[tspan(1),pE_0(1);tspan(1),pE_0(2);tspan(1),pE_0(3);...
         tspan(startCylinder),pE_1(1);tspan(startCylinder),pE_1(2);tspan(startCylinder),pE_1(3);...
         tspan(end),pE_2(1);tspan(end),pE_2(2);tspan(end),pE_2(3)],[],[])

figure() 
subplot(3,1,1)
   plot_compare(0,tspan,y_dmp_old(1,:),y_dmp_new(1,:),[],'Time','Tool Position',...
                     {'X-prev';'X-new'},'Tool Position',xtickstep,[tspan(1),pE_0(1);...
                     tspan(startCylinder),pE_1(1);tspan(end),pE_2(1)],[],[])      
subplot(3,1,2)
   plot_compare(0,tspan,y_dmp_old(2,:),y_dmp_new(2,:),[],'Time','Tool Position',...
                     {'Y-prev';'Y-new'},'Tool Position',xtickstep,[tspan(1),pE_0(2);...
                     tspan(startCylinder),pE_1(2);tspan(end),pE_2(2)],[],[])     
subplot(3,1,3)
plot_compare(0,tspan,y_dmp_old(3,:),y_dmp_new(3,:),[],'Time','Tool Position',...
                     {'Z-prev';'Z-new'},'Tool Position',xtickstep,[tspan(1),pE_0(3);...
                     tspan(startCylinder),pE_1(3);tspan(end),pE_2(3)],[],[])  
%%
clear F_den F_num g hc hd i jacobian k len len1 len2 tau q_d q_dot R_0E ...
      X1 Y1 Z1 X Y Z xtickstep ans;
